# Elucidation

[![pipeline status](https://gitlab.com/fortitudetec/elucidation-project/elucidation/badges/master/pipeline.svg)](https://gitlab.com/fortitudetec/elucidation-project/elucidation/-/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=com.fortitudetec%3Aelucidation&metric=alert_status)](https://sonarcloud.io/dashboard?id=com.fortitudetec%3Aelucidation)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=com.fortitudetec%3Aelucidation&metric=coverage)](https://sonarcloud.io/dashboard?id=com.fortitudetec%3Aelucidation)
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

Elucidation is a set of tools to allow for the visualization of the connections between microservices that make up an 
environment.  The connections that are currently supported are REST based and JMS (messaging) based.
### Projects
* [Bundle](elucidation-bundle/README.md)
* [Client](elucidation-client/README.md)

---
Copyright (c) 2018 - 2020 Fortitude Technologies, LLC
